const postcssNesting = require('postcss-nesting')
const postcssAutoprefixer = require('autoprefixer')
const postcssPurgeCSS = require('@fullhuman/postcss-purgecss')
const postcssNano = require('cssnano')
const postcssFlexbugsFixes = require('postcss-flexbugs-fixes')
const postcssPresetEnv = require('postcss-preset-env')

module.exports = {
  plugins: [
    postcssNesting(),
    postcssAutoprefixer(),
    postcssPurgeCSS({
      content: [
        './src/**/*.astro',
        './src/**/*.tsx',
        './src/**/*.vue',
        './src/**/*.svelte',
        './src/**/*.html',
      ],
    }),
    postcssNano({
      preset: 'default',
    }),
    postcssFlexbugsFixes(),
    postcssPresetEnv({
      stage: 3,
      autoprefixer: {
        grid: true,
      },
      features: {
        'nesting-rules': false,
      },
    }),
  ],
}
