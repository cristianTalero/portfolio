module.exports = {
  '/src/**/*.(astro|tsx|ts)': () => 'npx tsc --noEmit',

  'src/**/*.(astro|tsx|js|ts)': filenames => [
    `npx eslint --fix ${filenames.join(' ')}`,
    `npx prettier --write --plugin-search-dir=. ${filenames.join(' ')}`,
  ],

  '**/*.(md|json|yml|yaml)': filenames =>
    `npx prettier --write ${filenames.join(' ')}`,
}
