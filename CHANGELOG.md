# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.12](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.11...v0.4.12) (2023-05-03)


### 🐛 Bug Fixes

* **pages:** section link reload fixed ([9eab421](https://gitlab.com/cristianTalero/portfolio/commit/9eab421f520f62e60db7f26e7b90796bc64f51fe))

### [0.4.11](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.10...v0.4.11) (2023-05-03)


### 🐛 Bug Fixes

* **src/Layout:** html lang modified to content lang ([35628a1](https://gitlab.com/cristianTalero/portfolio/commit/35628a1a900a19dc6fd9133ab994bc37dfb65ff7))


### 🚚 Chores

* **public:** 'cv' project image replaced with one more actual ([fe8058b](https://gitlab.com/cristianTalero/portfolio/commit/fe8058ba20595c78394f2f0863add3416d4fe3c0))
* **public/images:** 'ecommerce' project info and captures replaced ([b003c63](https://gitlab.com/cristianTalero/portfolio/commit/b003c634b99c6ac10f031d400f9b4f3e8f47a540))

### [0.4.10](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.9...v0.4.10) (2023-04-27)


### 🚚 Chores

* notion cv removed from footer links ([8878e5a](https://gitlab.com/cristianTalero/portfolio/commit/8878e5a8502c81f736e5b7a5166936a700cd506e))
* **pages/precios:** 'example' label font size modified ([545c6fc](https://gitlab.com/cristianTalero/portfolio/commit/545c6fc29b16044823b7ef94bdd3ef3e23cbd7a1))

### [0.4.9](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.8...v0.4.9) (2023-04-27)


### 🚚 Chores

* **public/docs:** curriculum pdf updated ([9ae7049](https://gitlab.com/cristianTalero/portfolio/commit/9ae704915e2c8eece03a8a122d05fe5bd200e645))
* **public/images:** profile photo replaced ([c831095](https://gitlab.com/cristianTalero/portfolio/commit/c831095507461901a18da6502cf5b23731d3013e))
* skills modified ([0771f26](https://gitlab.com/cristianTalero/portfolio/commit/0771f26ec18bcff076f6d79fc35e74776304758a))

### [0.4.8](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.7...v0.4.8) (2023-04-15)


### 🚚 Chores

* **pages/precios.astro:** example label added to prices page ([68a733f](https://gitlab.com/cristianTalero/portfolio/commit/68a733f8a0da71fd99f4b4e8d87128e85a1c9733))


### ✨ Features

* **components/layout/Footer.astro:** github added to social section ([8f9faf3](https://gitlab.com/cristianTalero/portfolio/commit/8f9faf3b5a85ad9589421b5a0544d089f7683c7b))

### [0.4.7](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.6...v0.4.7) (2023-03-15)


### 🚚 Chores

* pdf files were compressed ([bd01b0b](https://gitlab.com/cristianTalero/portfolio/commit/bd01b0b529db1a541208cc5a15de23bd34cac2a2))

### [0.4.6](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.5...v0.4.6) (2023-03-10)


### 🚚 Chores

* added 'ecommerce' screenshots to project ([29d2090](https://gitlab.com/cristianTalero/portfolio/commit/29d2090b580b642c5d76a2036f0e796d9ec573e3))
* payloadcms removed from skills section ([c6effa7](https://gitlab.com/cristianTalero/portfolio/commit/c6effa7d8312488562ec493eb57898ad6fc3c3d6))
* **public/docs/Curriculum:** curriculum updated ([8a7fe29](https://gitlab.com/cristianTalero/portfolio/commit/8a7fe2956b85e436c856081976480245fd45bcbd))
* **public/docs/University.pdf:** university certificate was updated ([d819570](https://gitlab.com/cristianTalero/portfolio/commit/d8195702d60eb3af846be995a92d842cd595f21f))

### [0.4.5](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.4...v0.4.5) (2023-01-16)


### 🚚 Chores

* 'merge' hooks removed ([9168569](https://gitlab.com/cristianTalero/portfolio/commit/9168569adf72827f1e7d534ac3446b4231873ef7))


### 🐛 Bug Fixes

* **pages/proyectos/[slug]:** proyect links wrap icon fixed ([01871cc](https://gitlab.com/cristianTalero/portfolio/commit/01871cca8d4d96d97ee5caf499c06e9d77b54f0d))

### [0.4.4](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.3...v0.4.4) (2023-01-04)


### 🚚 Chores

* **data/projects.json:** firebase added to 'ecommerce' and 'social-network' projects ([be860d4](https://gitlab.com/cristianTalero/portfolio/commit/be860d433e0986e09e7cc207115574b7edf61cb0))


### 🐛 Bug Fixes

* **components/client/themetoggle.tsx:** transition fixed ([7976f3f](https://gitlab.com/cristianTalero/portfolio/commit/7976f3f6345163cb39cc64fa3732496cd95b5360))
* **pages/projects/[slug].astro:** sections link fixed ([6d654c8](https://gitlab.com/cristianTalero/portfolio/commit/6d654c8e1a841cde0262f0439bbaf9e041171204))

### [0.4.3](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.2...v0.4.3) (2023-01-03)


### 🚚 Chores

* **.husky:** git merge hooks modified ([18f3079](https://gitlab.com/cristianTalero/portfolio/commit/18f3079a2d5840b84981e74405f95d2cbcdc5fa0))
* **data/projects.json:** 'isOnline' property removed from projects ([5cf4813](https://gitlab.com/cristianTalero/portfolio/commit/5cf4813af70565ed8bc554a3bd27a3382871b1b6))
* **icons/education/udemy.svg:** icon color changed ([4ced834](https://gitlab.com/cristianTalero/portfolio/commit/4ced8340865602567c7b99fdea55341f6e2fcb3e))
* **package.json:** jest removed ([8e55df6](https://gitlab.com/cristianTalero/portfolio/commit/8e55df64bd714fcca89163c7b238fac7b8b20381))


### 🐛 Bug Fixes

* **components/cards/projectcard.astro:** preview image bug fixed ([e9eb416](https://gitlab.com/cristianTalero/portfolio/commit/e9eb4165deb1d564e5897ef3cdd0d23adadf3400))
* **layout.astro:** 'main' top padding fixed ([6ce87b9](https://gitlab.com/cristianTalero/portfolio/commit/6ce87b95431723af2e4aafbdd6b16ccb05afeb65))
* **pages/index.astro:** 'scroll-reveal' no working in index.astro fixed ([fbbcbee](https://gitlab.com/cristianTalero/portfolio/commit/fbbcbeed34043bb66d0e68ce1ba67813738a9c92))


### 💄 Styling

* **components/client/themetoggle.tsx:** component styles modified ([e1c00d8](https://gitlab.com/cristianTalero/portfolio/commit/e1c00d8652a948c2d4ed55dab50886e09dbc605d))
* **pages/projects/[slug].astro:** icon link added ([3cbfcec](https://gitlab.com/cristianTalero/portfolio/commit/3cbfcec52b208a0a80c6426cb69a9c6c05bda71f))
* **pages/proyectos/[slug].astro:** technology section is responsive now ([de25b66](https://gitlab.com/cristianTalero/portfolio/commit/de25b66ae00e42be44246d7b9ccb2e4c41b20303))


### ✨ Features

* 'is development' flag added to project page ([a70e407](https://gitlab.com/cristianTalero/portfolio/commit/a70e407e359bb4361be9db9125cb722e17cb62b7))
* 'portfolio' project finished ([3e81468](https://gitlab.com/cristianTalero/portfolio/commit/3e81468cd9db01fe31b76dca76e209f6115d83c0))
* **pages/projects/[slug].astro:** repository link added to project page ([6bdf2f1](https://gitlab.com/cristianTalero/portfolio/commit/6bdf2f1372fe41404ceaef89150f76c1dc7915ba))
* **pages/proyectos/[slug].astro:** 'single project page' added ([bf8a278](https://gitlab.com/cristianTalero/portfolio/commit/bf8a27881aca6ff9526969facfdff56586fb986b))

### [0.4.2](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.1...v0.4.2) (2022-12-31)


### ✨ Features

* **components/client/themetoggle.tsx:** theme toggle added ([b07b926](https://gitlab.com/cristianTalero/portfolio/commit/b07b926950f794b60f30154513be2880f01ee77a))
* dark theme v1 added ([ed5fdff](https://gitlab.com/cristianTalero/portfolio/commit/ed5fdffdce83179660c071ada37fe8df18618007))


### ♻️ Code Refactoring

* **components/client/projectstatus.tsx:** project status in a external component ([b72de16](https://gitlab.com/cristianTalero/portfolio/commit/b72de161b4ecf02856cdb1d1133cece2290cb138))


### 🚚 Chores

* **.husky/post-merge:** 'post-merge' git hook added ([77d230e](https://gitlab.com/cristianTalero/portfolio/commit/77d230ed5f098f12c9f40b268805dd2265a91ae1))
* new contact email added to 'footer social' and pdf curriculum ([ce45528](https://gitlab.com/cristianTalero/portfolio/commit/ce45528ed7817cf5f32329be79c07db1f194b460))
* **package.json:** unnecessary scripts removed ([0ab61f9](https://gitlab.com/cristianTalero/portfolio/commit/0ab61f94073ec5f9f2a1ec8093cd7c482c5877af))


### 🐛 Bug Fixes

* **functions/recatpcha.ts:** post request updated ([824e968](https://gitlab.com/cristianTalero/portfolio/commit/824e96868e5d60786f5bc8fe8f316e8d17287d3b))

### [0.4.1](https://gitlab.com/cristianTalero/portfolio/compare/v0.4.0...v0.4.1) (2022-12-31)


### ✨ Features

* **pages/contacto.astro:** contact form working ([e90bc40](https://gitlab.com/cristianTalero/portfolio/commit/e90bc40b72dee3acf2de68714a352b74fc81fa7b))

## [0.4.0](https://gitlab.com/cristianTalero/portfolio/compare/v0.3.3...v0.4.0) (2022-12-29)


### ⚠ BREAKING CHANGES

* open graph added for all pages
* **astro.config.mjs:** Better SEO

### 💄 Styling

* **components/layout/header.astro:** new styles to header component ([0de29fe](https://gitlab.com/cristianTalero/portfolio/commit/0de29fe47ab15159561e85babd8d797af591db79))


### 🚚 Chores

* **.versionrc:** .versionrc and CHANGELOG.md removed ([3a18351](https://gitlab.com/cristianTalero/portfolio/commit/3a1835182f8b093bc0a8c9489a9674f6433290ed))
* **functions:** functions folder removed ([c5292b5](https://gitlab.com/cristianTalero/portfolio/commit/c5292b589db6978dd752160786e6c7d1e2766426))
* **package.json:** 'astro-seo' missing package added ([21bd969](https://gitlab.com/cristianTalero/portfolio/commit/21bd969503e13a610ef62934c13517d2896de0ca))
* **package.json:** 'astro-seo' missing package added ([cbb2c0b](https://gitlab.com/cristianTalero/portfolio/commit/cbb2c0be4e9f604e114e59123118eb80d8bbc4f4))
* **package.json:** new script added ([0b3ca45](https://gitlab.com/cristianTalero/portfolio/commit/0b3ca45885dd4ab297fc0010e97e1340d53ebbad))
* **package.json:** scripts removed ([31ef971](https://gitlab.com/cristianTalero/portfolio/commit/31ef971b1865001c8b0f0c43c77159f21b2fc709))
* **public/google*.html:** search console verification added ([f09693f](https://gitlab.com/cristianTalero/portfolio/commit/f09693fd473565b24d673493886030f81ddcc755))


### 🐛 Bug Fixes

* **layout.astro:** open graph image bug fixed ([f83d8e5](https://gitlab.com/cristianTalero/portfolio/commit/f83d8e5d11196450c8ba8c713e28f1cee734acc5))


### ⚡️ Performance Improvements

* 'hero' and 'me' images format changed ([d99957a](https://gitlab.com/cristianTalero/portfolio/commit/d99957a4254ba93e8e099a10426aea32685bdcc3))
* accessibility and SEO improved ([55e8d52](https://gitlab.com/cristianTalero/portfolio/commit/55e8d527ad0b4e87f5fc047bbf68cfdf8c6b2d5a))
* **astro.config.mjs:** sitemap integration added ([75c4fb0](https://gitlab.com/cristianTalero/portfolio/commit/75c4fb0d16dc7ab8f3a6875b94eb1488da7ba23d))
* **pages/projects/index.astro:** aria-label added to link ([72d2eea](https://gitlab.com/cristianTalero/portfolio/commit/72d2eea4dbeb160d630410fd262e8f6a8aa0c178))
* **public/docs/curriculum.pdf:** pDF file was compressed for a better performance ([a258abd](https://gitlab.com/cristianTalero/portfolio/commit/a258abd5b91ba14866831d842877d9c53eb37313))
* **public/docs/university.pdf:** pDF file compressed for a better performance ([ae93fda](https://gitlab.com/cristianTalero/portfolio/commit/ae93fdabaa2dfed3ee7158b7aace5fd45d85a5cb))
* **public/images/projects:** images format changed to WEBP ([f6395bd](https://gitlab.com/cristianTalero/portfolio/commit/f6395bdf80022c42b979784bbbbb2dd8e768fc59))
* **public/robots.txt:** sitemap url added to robots file ([48162cc](https://gitlab.com/cristianTalero/portfolio/commit/48162cc7a840ab002f2a256dd0028cdb8e493a3c))
* seo improves ([2c39c5e](https://gitlab.com/cristianTalero/portfolio/commit/2c39c5eb5544f8a9bb12e6ad39f61769dbcba562))


### ✨ Features

* cV in PDF format and typescript paths added ([8447001](https://gitlab.com/cristianTalero/portfolio/commit/8447001f8ae5c60470855420c6f223f6cf30fcc6))
* main pages descriptions updated ([694d0a6](https://gitlab.com/cristianTalero/portfolio/commit/694d0a66613e5cb2504a63909f23b8cb91087278))
* page completely translated to spañish ([8323936](https://gitlab.com/cristianTalero/portfolio/commit/83239366e5d4d6a81054f791659393845ce02c6e))
* **pages/404.astro:** pathname removed ([513b62d](https://gitlab.com/cristianTalero/portfolio/commit/513b62d7d4c5d3c3b6bcd3e6c9c446b8662b7c7b))
* **pages/contact.astro:** sweetalert removed ([1d4da00](https://gitlab.com/cristianTalero/portfolio/commit/1d4da0023ff048dabe6819b065b51b3895e0168c))
* **public/images/projects:** multiple images for project added ([3609681](https://gitlab.com/cristianTalero/portfolio/commit/360968106367e88496ccf0323d8f446f96aa8d02))


### ♻️ Code Refactoring

* **components/cards/projectcard.astro:** unneccesary function removed ([1507959](https://gitlab.com/cristianTalero/portfolio/commit/1507959ba9fef0e96d4387dffe64dede239dc213))
* **data folder:** page data parsed to individual json file ([e82d198](https://gitlab.com/cristianTalero/portfolio/commit/e82d198545f4a22d94e93f666631c0ce5e1e6dcb))

### [0.3.3](https://gitlab.com/cristianTalero/portfolio/compare/v0.3.2...v0.3.3) (2022-12-28)


### 🚚 Chores

* **package.json:** new command added ([29f39c0](https://gitlab.com/cristianTalero/portfolio/commit/29f39c09b08fe2b9019304323f231c4340a15539))

### [0.3.2](https://gitlab.com/cristianTalero/portfolio/compare/v0.3.1...v0.3.2) (2022-12-28)


### ✨ Features

* **pages/contact.astro:** sweetalert added ([cdd87f6](https://gitlab.com/cristianTalero/portfolio/commit/cdd87f656b3d10686ed2162978a9b017909a410e))

### [0.3.1](https://gitlab.com/cristianTalero/portfolio/compare/v0.3.0...v0.3.1) (2022-12-28)


### ✨ Features

* **functions/recaptcha/index.ts:** cloudflare worker created to validate Recaptcha token ([9ad56f8](https://gitlab.com/cristianTalero/portfolio/commit/9ad56f8d2221c5d9c73d642a41debda4e4fd3036))

## [0.3.0](https://gitlab.com/cristianTalero/portfolio/compare/v0.2.2...v0.3.0) (2022-12-28)


### ⚠ BREAKING CHANGES

* **pages/contact.astro:** Explicit Cloudflare Worker removed

### ♻️ Code Refactoring

* **pages/contact.astro:** reCaptcha function added ([3392e55](https://gitlab.com/cristianTalero/portfolio/commit/3392e556bbccffe31d778d827990eb02465d710e))

### [0.2.2](https://gitlab.com/cristianTalero/portfolio/compare/v0.2.1...v0.2.2) (2022-12-27)


### 🐛 Bug Fixes

* **_routes.json:** cloudflare routing file created ([6600e68](https://gitlab.com/cristianTalero/portfolio/commit/6600e689463d31963ce7edc4a156f679f33e87e1))

### [0.2.1](https://gitlab.com/cristianTalero/portfolio/compare/v0.2.0...v0.2.1) (2022-12-27)


### 🐛 Bug Fixes

* **functions/recaptcha.ts:** hTTP method added to Cloudflare worker ([5ff257b](https://gitlab.com/cristianTalero/portfolio/commit/5ff257bf7db6d351c88b77dd38dc092e91748bc1))

## [0.2.0](https://gitlab.com/cristianTalero/portfolio/compare/v0.1.6...v0.2.0) (2022-12-27)


### ⚠ BREAKING CHANGES

* **pages/contact.astro:** ReCaptcha added to contact form

### ✨ Features

* **pages/contact.astro:** fisrt attempt to add ReCaptcha to 'Contact form' ([c4535c1](https://gitlab.com/cristianTalero/portfolio/commit/c4535c1c194e7e7a41d579fd69360be90b54cbae))

### [0.1.6](https://gitlab.com/cristianTalero/portfolio/compare/v0.1.5...v0.1.6) (2022-12-27)


### ♻️ Code Refactoring

* **tsconfig.json:** multiple file extentions in 'include' array ([7c394e2](https://gitlab.com/cristianTalero/portfolio/commit/7c394e29859f706d490ead25360f90cd2ddd38af))

### [0.1.5](https://gitlab.com/cristianTalero/portfolio/compare/v0.1.4...v0.1.5) (2022-12-27)


### 🚚 Chores

* **package.json:** push command added ([4f5290d](https://gitlab.com/cristianTalero/portfolio/commit/4f5290d84b5745333650244ab25425ec28450785))

### [0.1.4](https://gitlab.com/cristianTalero/portfolio/compare/v0.1.3...v0.1.4) (2022-12-27)


### 🐛 Bug Fixes

* **layout.astro:** header doesn't render on some pages ([c9c85cc](https://gitlab.com/cristianTalero/portfolio/commit/c9c85ccba4333f47cc5c3ef6e04aefb4b3373262))
* **layout.astro:** trailing slash bug fixed ([c3f586e](https://gitlab.com/cristianTalero/portfolio/commit/c3f586e96ceff068bb8a1ec96ac27ea9d84353ae))


### 💄 Styling

* **header.astrro:** cursive font removed ([ee61885](https://gitlab.com/cristianTalero/portfolio/commit/ee61885d8f045a8c22dfa9ea7cc99beb32bdaa38))
* **layout.astro:** 'Website in development' text flag was centering ([d5debc5](https://gitlab.com/cristianTalero/portfolio/commit/d5debc5e06ccd9a9c752d7a1e74a94204b18ab63))


### ✨ Features

* **components/cards/pricingcard.astro:** icon added to card ([4e15e64](https://gitlab.com/cristianTalero/portfolio/commit/4e15e64781da387c3af67a7255484e9e1086743e))

### [0.1.3](https://gitlab.com/cristianTalero/portfolio/compare/v0.1.2...v0.1.3) (2022-12-27)


### 🚚 Chores

* **components/header.astro:** debug console.log removed ([ae94b83](https://gitlab.com/cristianTalero/portfolio/commit/ae94b83cfba37030e8ef2a7e8d7b2279326defd7))

### 0.1.2 (2022-12-27)


### ✨ Features

* **website create:** principal routes an resources of the page was created 4313fa4
