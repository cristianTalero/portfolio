const corsHeaders = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'POST',
  'Access-Control-Allow-Headers': '*',
}

export interface Env {
  RECAPTCHA_SECRET: string
}

export default {
  async fetch(request: Request, env: Env, ctx: ExecutionContext) {
    try {
      if (request.method !== 'POST') {
        return new Response('Method not allowed', { status: 405 })
      }

      const data = await request.json<{ recaptcha: string }>()

      const secret = env.RECAPTCHA_SECRET
      const response = data.recaptcha
      const recaptchaURL = 'https://www.google.com/recaptcha/api/siteverify'

      const formData = new FormData()
      formData.append('secret', secret)
      formData.append('response', response)

      const res = await fetch(recaptchaURL, {
        method: 'POST',
        body: formData,
        headers: { Accept: 'application/json' },
      })

      const responseData = await res.json()

      return new Response(JSON.stringify(responseData), {
        headers: { 'Content-Type': 'application/json', ...corsHeaders },
      })
    } catch (e) {
      return new Response((e as Error).message, {
        status: 500,
        statusText: 'An error ocurred',
      })
    }
  },
}
