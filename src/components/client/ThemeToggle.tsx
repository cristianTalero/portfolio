import { useState, useEffect } from 'preact/hooks'

const defaultTransition = 'transform .35s, background-color .35s'
const colors = {
  dark: '#1e293b',
  light: '#facc15',
}

export const ThemeToggle = () => {
  const [theme, setTheme] = useState(localStorage.getItem('theme') ?? 'light')
  const [darkTheme, setDarkTheme] = useState(false)

  const isDarkTheme = theme === 'dark'

  const handleClick = () => {
    setDarkTheme(toggle => !toggle)
    setTheme(isDarkTheme ? 'light' : 'dark')

    document.documentElement.style.transition =
      'background-color 0.3s, color 0.3s'
  }

  useEffect(() => {
    const classList = document.documentElement.classList

    if (isDarkTheme) {
      classList.add('dark')
      setDarkTheme(true)
    } else {
      classList.remove('dark')
      setDarkTheme(false)
    }

    localStorage.setItem('theme', theme)
  }, [theme])

  return (
    <>
      <div
        style={{
          borderRadius: '9999px',
          width: '4.5rem',
          height: '2rem',
          padding: '0.25rem',
          transition: defaultTransition,
          boxShadow: '0 0 10px gray',
          position: 'absolute',
          top: '1rem',
          right: '1rem',
          cursor: 'pointer',
          backgroundColor: darkTheme ? '#fef08a' : '#85929E',
        }}
        title={darkTheme ? 'Tema oscuro' : 'Tema claro'}
        onClick={handleClick}
      >
        <input
          aria-label="Activar modo oscuro"
          type="checkbox"
          checked={darkTheme}
          style={{ display: 'none' }}
        />
        <div
          style={{
            borderRadius: '9999px',
            width: '1.5rem',
            height: '1.5rem',
            padding: '0.25rem',
            transition: defaultTransition,
            transform: darkTheme ? 'translateX(2.5rem)' : null,
            backgroundColor: darkTheme ? colors.light : colors.dark,
          }}
        >
          {darkTheme ? (
            <span class="material-icons" style={{ color: colors.dark }}>
              dark_mode
            </span>
          ) : (
            <span class="material-icons" style={{ color: colors.light }}>
              wb_sunny
            </span>
          )}
        </div>
      </div>
    </>
  )
}
