import { useEffect, useState } from 'preact/hooks'

type ProjectStatusProps = {
  url: string
  size: number
}

const defaultShadow = (color: string) => {
  return `drop-shadow(2px 2px 4px ${color}`
}

export const ProjectStatus = ({ url, size }: ProjectStatusProps) => {
  const [isOnline, setIsOnline] = useState(false)

  const color = isOnline ? '#17da95' : '#ec2727'

  useEffect(() => {
    fetch(url)
      .then(_ => setIsOnline(true))
      .catch(_ => setIsOnline(false))
  }, [])

  return (
    <span
      style={{
        fontWeight: '700',
        marginTop: '5px',
        color: color,
        filter: defaultShadow(color),
      }}
    >
      <span
        class="material-icons"
        style={{
          verticalAlign: 'middle',
          marginRight: '5px',
          fontSize: `${size - 0.2}rem`,
        }}
      >
        circle
      </span>
      <span style={{ fontSize: `${size}rem` }}>
        {isOnline ? 'Online' : 'Offline'}
      </span>
    </span>
  )
}
